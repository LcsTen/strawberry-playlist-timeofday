use std::fs::File;
use std::io::{prelude::*, BufReader};
use std::process::Command;
use std::thread::sleep;
use std::time::Duration;
use std::env::args;

use chrono::{prelude::*, Local};

#[derive(Clone)]
struct Playlist {
    time: u32,
    playlist: String,
}

fn load_playlist(strawberry_path: &String, name: &String) {
    println!("Playing playlist '{}'", name);
    Command::new(strawberry_path)
        .arg("--play-playlist")
        .arg(name)
        .spawn()
        .expect(format!("Command '{} --play-playlist {}' failed", strawberry_path, name).as_str());
}

fn main() {
    let strawberry_path = {
        let args: Vec<String> = args().collect();
        if args.len() == 1 {
            String::from("strawberry")
        }else{
            args[1].clone()
        }
    };
    Command::new(&strawberry_path).arg("--help").output().expect(format!("Command '{} --help' failed", strawberry_path).as_str());
    let spt = File::open("spt.txt").expect("The file 'spt.txt' isn't found.");
    let reader = BufReader::new(spt);
    let mut playlists = Vec::<Playlist>::new();
    let mut line_number = 1;
    for line in reader.lines() {
        let line = line.expect("There was an error while reading 'spt.txt'.");
        let mut splits = line.split(' ');
        let hours_minutes = splits
            .next()
            .expect(format!("Split failed at line {}", line_number).as_str());
        let playlist = {
            let mut res = String::new();
            for split in splits {
                if !res.is_empty() {
                    res += " ";
                }
                res += split;
            }
            res
        };
        let mut hours_minutes_splits = hours_minutes.split(':');
        let hours = hours_minutes_splits
            .next()
            .expect(format!("Split of hours/minutes failed at line {}", line_number).as_str());
        let minutes = hours_minutes_splits
            .next()
            .expect(format!("Split of hours/minutes failed at line {}", line_number).as_str());
        let hours: u32 = hours
            .parse()
            .expect(format!("Can't convert hours to integer at line {}", line_number).as_str());
        let minutes: u32 = minutes
            .parse()
            .expect(format!("Can't convert minutes to integer at line {}", line_number).as_str());
        let time = hours * 60 * 60 + minutes * 60;
        playlists.push(Playlist { time, playlist });
        line_number += 1;
    }
    if playlists.is_empty() {
        return;
    }
    playlists.sort_by(|a, b| a.time.cmp(&b.time));
    let now = Local::now();
    let now = now.hour() * 60 * 60 + now.minute() * 60 + now.second();
    // Search for the playlist to be played; if none is found it means the last playlist must be played (the last from yesterday)
    let playlist = 'b: {
        let mut i = 0;
        for p in &playlists {
            if now < p.time {
                break 'b (if i == 0 {
                    &playlists.last().unwrap().playlist
                } else {
                    &playlists[i - 1].playlist
                });
            }
            i += 1;
        }
        &playlists.last().unwrap().playlist
    };
    load_playlist(&strawberry_path, playlist);
    loop {
        let now = Local::now();
        let now = now.hour() * 60 * 60 + now.minute() * 60 + now.second();
        // Search for the playlist to be played after this one; if none is found it means it is the first playlist must be played (the first of tomorrow).
        let playlist = 'b: {
            for p in &playlists {
                if now < p.time {
                    break 'b p;
                }
            }
            playlists.first().unwrap()
        };
        println!(
            "Next playlist '{}' at {}:{}",
            playlist.playlist,
            playlist.time / (60 * 60),
            (playlist.time / 60) % 60
        );
        println!("Now is {}:{}", now / (60 * 60), (now / 60) % 60);
        println!(
            "Sleeping for {} seconds",
            playlist
                .time
                .checked_sub(now)
                .unwrap_or(24 * 60 * 60 + playlist.time - now)
        );
        sleep(Duration::new(
            playlist
                .time
                .checked_sub(now)
                .unwrap_or(24 * 60 * 60 + playlist.time - now)
                .into(),
            0,
        ));
        load_playlist(&strawberry_path, &playlist.playlist);
    }
}
