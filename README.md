# strawberry-playlist-timeofday

strawberry-playlist-timeofday is a small background process that will load
playlist in Strawberry depending on the time of the day. When launched, it will
seek the file "spt.txt" in the working directory, which is a list of playlists
and the time of the day when the playlist must be launched. Here is an example:

```
08:00 Morning
12:00 Afternoon
16:00 Evening
20:00 Night
```

In this example, at 8:00, it'll load the playlist called "Morning", at 12:00,
it'll load the playlist "Afternoon", etc. Before 08:00, it'll load the playlist
"Night".

For it to work, it is mandatory that the playlist is already opened as a tab in
Strawberry; it isn't sufficient for the playlist to be in favorites (this is a
limitation of ``strawberry --play-playlist`` command, which is used behind the
scenes by strawberry-playlist-timeofday).

strawberry-playlist-timeofday accepts one argument, which is the path to the
strawberry executable. It defaults to ``strawberry``.
